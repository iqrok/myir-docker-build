#!/bin/bash

set -e
set -o nounset

if [ $# -ne 1 ] || ! [ -e $1 ]; then
	echo -e "Usage:\n $0 <disk>\n\nexample:\n $0 /dev/sdc\n\n"
	exit -1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DISK=$1
UBOOT=/root/source/u-boot

sudo dd if=${UBOOT}/u-boot.imx of=${DISK} seek=2 bs=512 conv=notrunc
sync
