#!/bin/bash

set -e
set -o nounset

ROOTFS_MOUNT=$1

if [ $# -ne 1 ] || ! [ -d $1 ]; then
	ROOTFS_MOUNT=/tmp/rootfs
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
KERNEL=/root/source/kernel
kernel_version=$(cat ${KERNEL}/include/config/kernel.release)

CROSS_PREFIX=arm-linux-gnueabihf-

install_dir="${ROOTFS_MOUNT}/lib/modules/${kernel_version}/"

# install kernel
pushd ${KERNEL}
rm -rf ${ROOTFS_MOUNT}/lib/modules/${kernel_version}/
make ARCH=arm CROSS_COMPILE=${CROSS_PREFIX} INSTALL_MOD_PATH=${ROOTFS_MOUNT} modules_install
popd
mkdir -p ${ROOTFS_MOUNT}/boot
cp -v ${KERNEL}/arch/arm/boot/zImage ${ROOTFS_MOUNT}/boot/vmlinuz-${kernel_version}
sh -c "echo 'uname_r=${kernel_version}' > ${ROOTFS_MOUNT}/boot/uEnv.txt"
sh -c "echo 'optargs=libphy.num_phys=2 console=tty0 quiet' >> ${ROOTFS_MOUNT}/boot/uEnv.txt"

DTB_FILE=mys-imx6ull-14x14-evk-emmc.dtb

# install device tree binary
mkdir -p ${ROOTFS_MOUNT}/boot/dtbs/${kernel_version}/
cp -a ${KERNEL}/arch/arm/boot/dts/${DTB_FILE} ${ROOTFS_MOUNT}/boot/dtbs/${kernel_version}/
sh -c "echo 'dtb=${DTB_FILE}' >> ${ROOTFS_MOUNT}/boot/uEnv.txt"

echo "DONE: $0!"
