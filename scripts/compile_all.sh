#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
MAKE_DIR=${DIR}/../scripts

cd ${MAKE_DIR}

make uboot
make kernel
