#!/bin/bash

set -e
set -o nounset

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ $# -ne 1 ] || ! [ -d $1 ]; then
	ROOTFS_MOUNT=/tmp/rootfs
fi

ROOTFS_MOUNT=$1
SCRIPT_PATH="`dirname \"$0\"`"

trap "sudo umount -f ${ROOTFS_MOUNT}/dev || true; exit" INT TERM EXIT

#debian rootfs
echo "Building debian rootfs..."

multistrap -f ${SCRIPT_PATH}/multistrap.conf -d ${ROOTFS_MOUNT}

cp /usr/bin/qemu-arm-static ${ROOTFS_MOUNT}/usr/bin/
cp -t ${ROOTFS_MOUNT}/etc/ /etc/resolv.conf /etc/passwd /etc/group /etc/shadow
cp ${SCRIPT_PATH}/install_rootfs_second_stage.sh ${ROOTFS_MOUNT}
chmod u+x ${ROOTFS_MOUNT}/install_rootfs_second_stage.sh
/bin/mknod -m 0666 ${ROOTFS_MOUNT}/dev/null c 1 3
/bin/mknod -m 0666 ${ROOTFS_MOUNT}/dev/random c 1 8
/bin/mknod -m 0444 ${ROOTFS_MOUNT}/dev/urandom c 1 9
sudo chroot ${ROOTFS_MOUNT} /bin/bash -c "./install_rootfs_second_stage.sh"

${SCRIPT_PATH}/50_install_kernel.sh ${ROOTFS_MOUNT}

sh -c "echo '/dev/mmcblk0p1        /       auto    errors=remount-ro       0       1' >> ${ROOTFS_MOUNT}/etc/fstab"

printf "auto lo
iface lo inet loopback

auto eth0 eth0:0
iface eth0 inet dhcp

iface eth0:0 inet static
address 192.168.0.92
netmask 255.255.255.0" > ${ROOTFS_MOUNT}/etc/network/interfaces

# install read only init script
cp ${SCRIPT_PATH}/init-ro ${ROOTFS_MOUNT}/sbin/
cp ${SCRIPT_PATH}/enable-rootfs-ro.sh ${ROOTFS_MOUNT}/usr/bin/

# remove chroot helpers
rm ${ROOTFS_MOUNT}/install_rootfs_second_stage.sh
rm ${ROOTFS_MOUNT}/etc/resolv.conf
rm ${ROOTFS_MOUNT}/usr/bin/qemu-arm-static

echo "DONE: Building debian rootfs!"
