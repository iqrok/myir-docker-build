#!/bin/bash
set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

IMAGE_TAG=myir-image-builder:latest
IMAGE_BASE_PATH=/root/source/scripts
IMAGE_ENTRYPOINT=${IMAGE_BASE_PATH}/build_sdimage.sh
IMAGE_BUILD_NAME=sdcard.img

TARGET_IMAGE_FILE=${DIR}/${IMAGE_BUILD_NAME}

rm -f ${TARGET_IMAGE_FILE}

docker build -t ${IMAGE_TAG} .
CONTAINER_ID=$(docker run -d --ipc="host" --privileged -t ${IMAGE_TAG} /bin/bash)

echo "STARTING CONTAINER ID ${CONTAINER_ID:0:12}"
docker exec ${CONTAINER_ID:0:12} ${IMAGE_ENTRYPOINT}

ALREADY_EXIST=1

while [ "${ALREADY_EXIST}" != 0 ]; do
	docker exec ${CONTAINER_ID:0:12} test -f /root/source/${IMAGE_BUILD_NAME}
	ALREADY_EXIST=$?
	sleep 1
done

echo "COPYING ${IMAGE_BUILD_NAME} FROM CONTAINER TO HOST"
docker cp ${CONTAINER_ID:0:12}:/root/source/${IMAGE_BUILD_NAME} ${DIR}/${IMAGE_BUILD_NAME}

echo "STOPPING CONTAINER ID ${CONTAINER_ID:0:12}"
docker stop ${CONTAINER_ID:0:12}

echo "REMOVING CONTAINER ID ${CONTAINER_ID:0:12}"
docker rm ${CONTAINER_ID:0:12}
