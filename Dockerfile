FROM ubuntu:18.04

RUN mkdir /root/source
RUN mkdir /root/source/kernel
RUN mkdir /root/source/u-boot

COPY MYiR-iMX-Linux.tar.bz2 /root/source
COPY MYiR-iMX-Uboot.tar.bz2 /root/source
COPY scripts/ /root/source/scripts

RUN tar xjf /root/source/MYiR-iMX-Linux.tar.bz2 -C /root/source/kernel
RUN tar xjf /root/source/MYiR-iMX-Uboot.tar.bz2 -C /root/source/u-boot

RUN sed -i -e "s/\r//g" /root/source/scripts/prepare.sh 
RUN sed -i -e "s/\r//g" /root/source/scripts/compile_all.sh

RUN apt-get update && apt-get install -y sudo gpg vim
RUN /root/source/scripts/prepare.sh
RUN /root/source/scripts/compile_all.sh

RUN git config --global user.name "gitlab-runner"
RUN git config --global user.email "gitlab-runner@example.com"
